import sqlite3
import csv

def connect():
    conn = sqlite3.connect('food_quality.db')
    c = conn.cursor()
    return c, conn

def insert_data():
    c, conn = connect()
    # Open the CSV file and read it using reader() function
    with open("/home/pcportable/Documents/python_docs/quality_food/quality_food/export_alimconfiance.csv", "r") as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=';')
    
    # Open the SQLite database and insert the records
        for row in csv_reader:
            # Create a tuple of values from each line in the csv file
            values = (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12])

            # Execute the sql query
            c.execute("INSERT INTO information VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)", values)

        # Commit the changes to the database
        conn.commit()
        conn.close()

if __name__ == '__main__':
    insert_data()