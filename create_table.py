import sqlite3

def connect():
    conn = sqlite3.connect('food_quality.db')
    c = conn.cursor()
    return c, conn

def create_table():
    c, conn = connect()
    c.execute('CREATE TABLE IF NOT EXISTS information (etablissement TEXT, siret INTEGER, adress TEXT, codepostal INTEGER, commune TEXT, inspection INTEGER, date TEXT, activite TEXT, synthese TEXT, agrement TEXT, geores TEXT, filtre TEXT, type TEXT)')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    create_table()